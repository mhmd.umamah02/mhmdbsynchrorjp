using System;
using FluentAssertions;
using Hanssens.Net;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FluentAssertions.Common;
using Hanssens.Net.Helpers;
using Xunit;
using LocalStorageTests.Stubs

namespace dotnetcore
{
    class Program
    {
        //initialize a local storage variable
        var storage = new LocalStorage();
        //initialize a dictionary and a list to be used later
        IDictionary<int, int> accounts = new Dictionary<int, string>();
        List<string> keys = new List<string>()

        static void Main(string[] args)
        {
            //initialize two accounts as an example
            var customerID1 = "1";
            var customerID2 = "2";

            var initialCredit1 = "500";
            var initialCredit2 = "1000";

            //store those accounts locally
            storage.Store(customerID1, initialCredit1);
            storage.Store(customerID2, initialCredit2);

            //user input customerID and initial credit
            Console.WriteLine("please enter the customerID: ");  
            int ID = Convert.ToInt32(Console.ReadLine()); 

            Console.WriteLine("please enter the initial credit");  
            int initial_credit = Convert.ToInt32(Console.ReadLine());

            //store the keys retrieved from the local storage in a list named keys
            keys = Object.keys(storage);
            //i is equal to the length of the list
            int i = keys.length;

            //add the accounts retrieved into a dictionary
            while ( i <= keys.length ) {
                accounts.Add( keys[i], Convert.ToInt32( storage.getItem( keys[i] ) ) );
                i++;
            }

            //find the account whose id is equal to that entered
            for (int i=0; i<accounts.length; i++){
                if (ID == author.Key[i]){

                    //if the initial credit is zero add a transaction by the user
                    if (author.Value[i] == 0){
                        Console.WriteLine("Your initial credit is zero, please enter a transaction amount to be added to the initial credit");
                        int trans = Convert.ToInt32(Console.ReadLine());
                        author.Value[i]+=trans;
                    }

                }
            }

            //list all the accounts found
            Console.WriteLine("Enter the ID of the account that you want to see its full information: ");
            int userID = Convert.ToInt32(Console.ReadLine());
            Console.Write(accounts.Key[userID], accounts.Value[userID]);

        }
    }
}
